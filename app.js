engineIsRunning = true;
transmissionIsWorking = true;
wheelsAreInflated = false;
function conditionOfTheCar() {
    return new Promise((resolve, reject) => {
        const errors = [];
        if (!engineIsRunning) {
            errors.push('Engine is not running!');
        }

        if (!transmissionIsWorking) {
            errors.push('Transmission is not working!');
        }

        if (!wheelsAreInflated) {
            setTimeout(() => {errors.push('Wheels are flat!')}, 0);
        }
        if (errors.length > 0){
            reject(errors);
        }
        else
        {
            setTimeout(() =>{
                resolve('Great! Car is ready.');
            }, 1000);
        }
    });
}

async function handleCarCondition() {
    try {
        const message = await conditionOfTheCar();
        console.log(message);
    } catch (errors) {
        errors.forEach(error => {
            console.log(error);
        });
    }
}

let promise = handleCarCondition();

/*
conditionOfTheCar().then((message) => {
    console.log(message);
}).catch((error) =>{
    console.log(error);
});
*/

/*
В чем суть?
1. Мы установили "wheelsAreInflated" = false => по идее, при проверки сообщение с этой ошибкой будет добавлено в массив с ошибками "errors".
2. Но т.к. мы установили задежку для добавления этого сообщения =>
=> будет выполнен последний else, без ожидания добавления ошибки
 */